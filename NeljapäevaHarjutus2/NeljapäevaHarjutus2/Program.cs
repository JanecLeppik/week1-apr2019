﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevaHarjutus2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tee omale 10x10 täisarvude massiiv int[10,10]
            const int R = 10;
            const int V = 12;
            int[,] arvud = new int[R, V];

            Console.WriteLine($"massiivi suurus on {arvud.Length}");
            Console.WriteLine($"see massiiv on {arvud.Rank} mõõtmeline");
            Console.WriteLine($"siin on {arvud.GetLength(0)} rida ja {arvud.GetLength(1)} veergu");


            //  täida see juhuslike arvudega 0..99
            Random r = new Random();
            for (int i = 0; i < R; i++) // ilus ei ole siia 10 kirjutada, äkki homme ta tahab muud arvu?
            {
                for (int j = 0; j < V; j++)
                {
                    arvud[i, j] = r.Next(100);  // tabelisse panime arvud 0..99
                }
            }

            // prindi see (kontrollimiseks) ekraanile 

            // nb! kahemõõtmelise massiivi "läbikäimiseks" kasutatakse sageli tüklit tsükli sees
            // kus üks tsüklimuutja (i) on rea ja teine (j) veeru number

            for (int i = 0; i < R; i++)
            {
                for (int j = 0; j < V; j++)
                {
                    Console.Write($"\t{arvud[i,j]}"); // mis kuradi asi see on, kes seletab?
                }
                Console.WriteLine();
            }

            // Siis küsi consolilt üks arv
            string vastus = ""; // meil on vaja muutujat kuhu consoolilt lugeda
            do
            {
                Console.Write("millist arvu otsid: ");
                ;
                if (int.TryParse(vastus = Console.ReadLine(), out int otsitav)) // see vajab pisut selgitamist - saaks ka teisiti
                {
                    // otsi see massiivist üles ja ütle ekraanile
                    // siin on otsitav olemas - hakkame otsima
                    if (otsitav < 100 && otsitav >= 0) // kas on üldse meie arv?
                    {
                        int mitu = 0; // paneme kirja, mitu leiame (alguses 0)
                        for (int i = 0; i < R; i++)
                        {
                            for (int j = 0; j < V; j++)
                            {
                                if (arvud[i, j] == otsitav) // kas leidsime
                                {
                                    // kui jah karjume välja ja suurendame leidude arvu
                                    Console.WriteLine($"leidsin arvu {otsitav} {i + 1}.reast ja {j + 1}.veerust ");
                                    mitu++;
                                }
                            }
                        }
                        // otsimine lõpetatud - vaatame tulemust
                        if (mitu == 0) // kui ei leidnud ühtegi
                        { Console.WriteLine("sellist arvu mul ei ole"); }
                        else // kui leidsin mõne
                        { Console.WriteLine($"seda arvu leidsin {mitu} tükki"); }

                    }
                    else { Console.WriteLine("arv liiga suur või väike"); }
                }

                else if (vastus != "") { Console.WriteLine("anna ikka miski arv"); }
            } while (vastus != "");
        }
    }
}
