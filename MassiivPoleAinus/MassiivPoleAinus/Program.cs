﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiivPoleAinus
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> arvud = new List<int> { 1, 2, 3, 4, 5 };

            arvud.Remove(3); // otsib üles selle asja ja nopib minema
            arvud.RemoveAt(3); // otsib niimitmenda ja nopib minema
            int[] teine = { 1, 3, 5, 7, 9 };
            var arvulist = teine.ToList();
            var arvuarray = arvulist.ToArray();

            arvud.AddRange(teine);
            arvud.AddRange(new List<int> { 9, 10, 11, 12 });

            if (arvud.Contains(17)) Console.WriteLine("17 on arvude hulgas olemas");

            for (int i = 0; i < arvud.Count; i++)
            {
                Console.WriteLine(arvud[i]);
            }

            // läheme edasi
            Console.WriteLine("\nsorteeritud list\n");
            SortedSet<int> sorteeritud = new SortedSet<int> { 1, 5, 2, 4, 3, 9, 2 };
            
            foreach (var x in sorteeritud) Console.WriteLine(x);

            // veel
            Console.WriteLine("\nstack ja queue\n");

            Stack<int> kuhi = new Stack<int>(); // pistetakse peale ja pealt nupitakse

            kuhi.Push(1);
            kuhi.Push(7);
            kuhi.Push(3);
            kuhi.Push(5);
            kuhi.Push(4);
            kuhi.Push(2);

            foreach (var x in kuhi) Console.WriteLine($"kuhjas on {x}");

            while(kuhi.Count > 0)
            {
                Console.WriteLine(kuhi.Pop());
            }

            Console.WriteLine("\njärjekord ehk queue\n");
            Queue<int> järjekord = new Queue<int>();

            järjekord.Enqueue(1);
            järjekord.Enqueue(7);
            järjekord.Enqueue(3);
            järjekord.Enqueue(5);
            järjekord.Enqueue(4);
            järjekord.Enqueue(2);

            while (järjekord.Count > 0)
            {
                Console.WriteLine(järjekord.Dequeue());
            }

            // sõnastik
            Console.WriteLine("\nsiit algab dictionary\n");

            Dictionary<int, string> nimekiri = new Dictionary<int, string>
            { { 1, "Henn"}, {2, "Ants"}, {3, "Peeter"} };

            nimekiri.Add(7, "Joosep");

            nimekiri.Add(9, nimekiri[3]);
            nimekiri.Remove(3);


            Console.WriteLine(nimekiri[9]);

            Dictionary<string, int> vanused = new Dictionary<string, int>();
            vanused.Add("Henn", 64);
            vanused.Add("Ants", 40);
            vanused.Add("Peeter", 28);

            vanused["Peeter"] = 66;

            Console.WriteLine(vanused["Peeter"]);

            foreach (var x in nimekiri) Console.WriteLine(x);

            foreach (var x in nimekiri.Values) Console.WriteLine(x);
            foreach (var x in nimekiri.Keys) Console.WriteLine(x);

            foreach(var x in nimekiri)
                if (x.Value == "Peeter") Console.WriteLine($"tema võti on {x.Key} ");

            foreach(var x in nimekiri.Keys)
            {
                Console.WriteLine($"võtme all {x} on {nimekiri[x]}");
            }

            if (nimekiri.ContainsKey(3)) Console.WriteLine(nimekiri[3]);
            else Console.WriteLine($"{3} võtmega kedagi pole");


        }
    }
}
