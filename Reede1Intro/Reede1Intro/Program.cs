﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reede1Intro
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Peitu
            // mõned tehted stringidega - ikka läheb vaja

            string testimiseks = "Henn tahab teada, mida stringiga teha saab";

            // ToUpper() ja ToLower()
            Console.WriteLine(testimiseks.ToLower());

            // Substring
            Console.WriteLine(testimiseks.Substring(7, 10));

            string imelik = "       see on      üks   imelik  jutt      ";
            Console.WriteLine($"->{imelik}<-");
            Console.WriteLine($"->{imelik.Trim()}<-");

            // string Split
            string[] sõnad = testimiseks.Split(' ');
            foreach (var x in sõnad) Console.WriteLine(x);

            // string join
            string vastus = String.Join(" ", sõnad);
            Console.WriteLine(vastus);

            // string Concat - panem massiivi niisama kokku
            Console.WriteLine(String.Concat(sõnad));
            

            // tahaks teha funktsiooni (?), mis teisendab esitähe suureks
            // henn -> Henn, HENN -> Henn

            Console.WriteLine("\npeale peitust\n");

            Console.WriteLine(ToProper("esialgne SÕNA"));

            string h = ToProper("");
            #endregion
        }

        // funktsioon Nimi: ToProper
        //            Vastuse tüüp: string
        //            Parameeter, mis on string süüpi
        static string ToProperx(string sõna)
        {
            //if (sõna == "") return "";

            return
                sõna == "" ? "" :
                sõna.Substring(0, 1).ToUpper() + sõna.Substring(1).ToLower();
        }



        static int Liida(int x, int y)
        {
            return x + y;
        }



    }
}
