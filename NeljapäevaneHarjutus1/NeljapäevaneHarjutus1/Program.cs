﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevaneHarjutus1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] esimene = { 2, 7, 3, 8, 9, 1, 0, 11, 12, 13 }; // esimene massiiv korrutamiseks
            int[] teine =   { 5, 7, 2, 11, 5, 8, 2, 11, 14, 16}; // teine massiiv
            int vastuseid = 0; // paneme vastamiste loenduri paika - iga kord vastamisel suurendame
            for (int i = 0; i < esimene.Length; i++) // käime massiivid läbi (mõlemad ühepikkused)
            {
                int vastus; // muutuja milles hoida vastust, et siis kontrollida saaks 
                            // peab olema ENNE tsüklit, muidu ei saa while fraasis kasutada
                
                do
                {
                    Console.Write($"kaks arvu {esimene[i]} ja {teine[i]}, mis on korrutis: ");
                    vastus = int.Parse(Console.ReadLine()); // loeme vastuse
                    vastuseid++; // loendame vastamisi
                }
                while (vastus != esimene[i] * teine[i]); // teeme niikaua, kuni vastus on vale!
                                                         // õige vastuse puhul piirdume 1 korraga
            }

            // hindamine - nüüd meil on vastamiste arv ja massiivi pikkkus - väike arvutus ja anname hinde
            int tulemus = esimene.Length * 100 / vastuseid;
            Console.WriteLine($"tulemus {tulemus} küsimusi {esimene.Length} vastuseid {vastuseid}");
            if (tulemus >= 90) Console.WriteLine("hinne 5");
            else if (tulemus >= 80) Console.WriteLine("hinne 4");
            else if (tulemus >= 70) Console.WriteLine("hinne 3");
            else if (tulemus >= 60) Console.WriteLine("hinne 2");
            else Console.WriteLine("hinne 1");
        }
    }
}
