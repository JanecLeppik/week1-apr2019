﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* 
Create Main-method, that gets color as argument
Or read color from console. 
According to the argument, there will be printed sentence:
Color = GREEN or green – „Driver can drive a car.“
Color = YELLOW or yellow – „Driver has to be ready to stop the car or to start driving.“
Color = RED or red – „Driver has to stop car and wait for green light“

Try to solve this task with if-else statements and after that repeat it with switch statement.
When You like - try Elvis method too ?: 
*/
namespace KolmapäevHarjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("mis värvi sa näed (green, red, yellow): ");
            string color = Console.ReadLine().ToLower();

            switch (color)
            {
                case "red":
                case "punane":
                    Console.WriteLine("jää seisma");
                    break;
                case "green":
                case "roheline":
                    Console.WriteLine("sõida edasi");
                    break;
                case "yellow":
                case "kõllane":
                    Console.WriteLine("oota rohelist");
                    break;
                default:
                    Console.WriteLine("pese silmad");
                    break;
            }

            if (color == "red" || color == "punane")
            {
                Console.WriteLine("jää seisma");
            }
            else if (color == "green" || color == "roheline")
            {
                Console.WriteLine("sõida edasi");
            }
            else if (color == "yellow" || color == "kõllane")
            {
                Console.WriteLine("oota rohelist");
            }
            else
            {
                Console.WriteLine("pese silmad");
            }

            Console.WriteLine(
                color == "red" || color == "punane" ? "jää seisma" :
                color == "green" || color == "roheline" ? "sõida edasi" :
                color == "yellow" || color == "kõllane" ? "oota rohelist" : "pese silmad"
                );

        }
    }
}
