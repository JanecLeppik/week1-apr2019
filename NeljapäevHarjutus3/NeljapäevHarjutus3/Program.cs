﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevHarjutus3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region peitu
            //DateTime dt = new DateTime(1955, 3, 7);
            //Console.WriteLine(dt.ToLongDateString());

            //DateTime täna = DateTime.Now;
            //Console.WriteLine(DateTime.Today);
            //Console.WriteLine(DateTime.Now);

            //while (true)
            //{

            //    Console.Write("ütle kuupäev: ");
            //    DateTime x = DateTime.Parse(Console.ReadLine());
            //    Console.WriteLine(x);

            //} 
            #endregion

            /*Küsi järjest Consoli pealt
1. anna mulle nimi
2. millal ta sündinud on

(kui 1. on tühi == "" - siis lõpetad)

antud vastused pane miskisse kollektsiooni 
(või kollektsioonidesse kuidas mugavam)

Kui kõik läbi
* leia, kes on kõikse vanem
* kellel järgmisena tulemas sünnipäev   -- sellega on natukene tegemist!
*/

            Dictionary<string, DateTime> sünnipäevad = new Dictionary<string, DateTime>();


            while(true)
            {
                Console.Write("Anna nimi: ");
                string nimi = Console.ReadLine(); if (nimi == "") break;
                Console.Write("Millal ta sündinud on: ");

                if (DateTime.TryParse(Console.ReadLine(), out DateTime sündinud))
                {
                    sünnipäevad.Add(nimi, sündinud);
                }
                else Console.WriteLine("vigane sünnipäev, kirja ei pane");
            }

            foreach (var x in sünnipäevad) Console.WriteLine(x);

            DateTime vanim = sünnipäevad.Values.Min();
            Console.WriteLine(vanim);

            foreach (var x in sünnipäevad)
            {
                if (x.Value == vanim) Console.WriteLine($"vanim on {x.Key}");
            }

        }
    }
}
