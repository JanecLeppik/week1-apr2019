﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiSegamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();

            // 	tee omale massiiv (või mingi muu asi) intidest, kus arvud
            //  kokku 52 tükki, arvud 0..51

            //int[] arvud = Enumerable.Range(0, 52).ToArray();
            List<int> kaardid = Enumerable.Range(0, 52).ToList();

            // trükkida välja 4 reas
            for (int i = 0; i < 52; ) Console.Write($"\t{kaardid[i]}{(++i % 13 == 0 ? "\n" : "")}");

            // mõtle välja kuidas nad sassi ajada

            // üks variant, kuidas teha on nii - teeme teise kuhja (paki), mis on tühi
            // ja siis võtame esimezsest kuhjast juhusliku kaardi ja paneme teise

            Console.WriteLine("\nhakkame segama\n");

            List<int> segatud = new List<int>();  // siin pole nüüd ühtegi kaarti

            while(kaardid.Count > 0)     // teeme nii kaua, kuni pakk on otsas
            {
                int mitmes = r.Next(kaardid.Count);  // võtame juhusliku kaardi 
                // segatud.Add(kaardid[mitmes]);        // paneme selle kaardi teise pakki
                // kaardid.RemoveAt(mitmes);            // korjame ta esimeset pakist ära

                int kaart = kaardid[mitmes];        // see on see, mille ümber tõstame
                kaardid.Remove(kaart);
                segatud.Add(kaart);

            }
            for (int i = 0; i < 52;) Console.Write($"\t{segatud[i]}{(++i % 13 == 0 ? "\n" : "")}");


        }
    }
}
