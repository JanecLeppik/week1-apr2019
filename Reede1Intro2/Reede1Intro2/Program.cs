﻿using System;
using System.Collections.Generic;
using System.IO;  // see on oluline rida
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reede1Intro2
{
    class Program
    {
        static void Main(string[] args)
        {
            // mõned tehted tekstifailidega

            string failinimi1 = "..\\..\\test.txt";      // siin rääkisime faili nimedest (string)
            string failinimi2 = @"..\..\MinuAndmed.txt";
            string nimedefail = @"..\..\nimed.txt";

            string loetud = File.ReadAllText(failinimi1);  // loe tekst tekstifailist (harva)
            //Console.WriteLine(loetud);

            string[] loetudRead = File.ReadAllLines(failinimi1);  // loe read tekstifailist (sageli)
            foreach (var x in loetudRead) Console.WriteLine(x);

            string[] nimed = { "Henn", "Ants", "Peeter", "Jaak" };
            File.WriteAllLines(nimedefail, nimed);  // nimede faili nimi, nimede massiiv
                                            // kirjuta stringimassiivi sisu kettale


        }
    }
}
