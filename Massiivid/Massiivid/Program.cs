﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            // siiani oli elu lill - nüüd hakkab keeruline elu
            // järgmist teemat me jätkame homme ja ka kauges tulevikus

            int[] arvud = new int[10]; // mis tüüpi asjaga on tegu - massiiv e array

            arvud[3] = 7; // massiivi element nr 3 (alates 0st) saab väärtuse

            Console.WriteLine(arvud[5]);

            int[] veel = { 7, 2, 3, 9, 1, 5, 12 };  // initsialiseeritud massiiv

            int[] kolmas;   // selle massiivil ei ole väärtus
            kolmas = new int[] { 1 * 17, 2 + 8, 3 / 4 }; // no anname talle (siin on new int[] kohustuslik)

            string[] nimed = { "Henn", "Ants", "Peeter" };  // siin on stringi tüüpi avaldised

            string[] päevad =
            {
                "esmaspäev",
                "teisipäev",
                "kolmapäev",
                "neljapäev",
                "reede",
                "laupäev",
                "pühapäev"
            };

            Console.WriteLine(päevad.Length);

            int[][] misseeon = new int[4][];
            misseeon[0] = new int[] { 1, 2, 3 };
            misseeon[1] = new int[] { 4, 5 };
            misseeon[2] = new int[] { 6, 9, 10 };
            misseeon[3] = arvud;

            Console.WriteLine(misseeon[2][2]);

            int[][] tabel =
            {
                new int[] { 1, 2, 3 },
                new int[] { 4, 5 },
                new int[] { 6,9,10 },
                arvud,
                misseeon[2]
            };

            int[,] ruut = new int[10, 10];

            Console.WriteLine(tabel.Length);
            Console.WriteLine(ruut.Length);
            Console.WriteLine(tabel.Rank);

            int[,] teineRuut = { { 1, 2, 3 }, { 1, 2, 7 }, { 3, 4, 8 }, { 7, 5, 1 } };

            // massiividest räägime homme
            // täna rohkem ei räägi!

        }
    }
}
