﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolmapäev1
{
    class Program
    {
        static void Main(string[] args)
        {
            // eile jäi meil rääkimata STRING

            //string nimi = "Henn"; // kirjutaks midagi siia juurde
            //string teine = "henn";

            //bool kas = nimi.Equals(teine, StringComparison.CurrentCultureIgnoreCase);  //nimi == teine;
            //Console.WriteLine(kas);



            long l = 17;
            int i = (int)l;  // casting operation - ilmutatud teisendus
            long l2 = i;        // ilmutamata (peidetud) teisendus
            Console.WriteLine(i);

            string arvumoodi = "123";
            int arv1 = int.Parse(arvumoodi);
            // kasutame ära int klassi oskust end tekstist välja lugeda

            int arv2 = Convert.ToInt32(arvumoodi);
            // kasutame ära spetsiaalseid teisendusfunktsioone Convert.Toxxx

            //Console.Write("Palju sa palka saad: ");
            //            int palk = int.Parse(Console.ReadLine());

            //if (int.TryParse(Console.ReadLine(), out int palk))
            //Console.WriteLine($"sinu palk on siis {palk} aga võiks olla {palk*2}");
            //else Console.WriteLine("ah mine sa oma jutuga");

            string vastus = 7.ToString();
            Console.WriteLine(vastus);
        }
    }
}
