﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolmapäev2
{
    class Program
    {
        static void Main(string[] args)
        {

            #region selle osa peidame ära
            // see ja eelmine läksid üles ka GitLabi projekt
            // https://gitlab.com/Hennsarv/week1-apr2019.git

            // räägime natuke veel stringist ja ToStringist()
            // Console.WriteLine                    NB! kasuta code snippetit cw
            // Console.Write
            // Console.ReadLine
            // Console.ReadKey

            // String.Format 


            //Console.WriteLine("mõtle (jätkamiseks vajuta klahvi nimega \"any key\"");
            //Console.ReadKey(true);
            //Console.WriteLine("no mõtlesid järele");

            // siin on jutt interpoleeritud stringist ja placeholderitest
            int a = 7; int b = 8;
            Console.WriteLine("sul on kaks arvu a = {0} ja b = {1} ja nende korrutis on {2}", a, b, a * b);
            //string välja = "sul on kaks arvu a = " + a.ToString() + " ja b = " + b.ToString();
            //string välja = string.Format("sul on kaks arvu a = {0} ja b = {1}", a, b);
            string välja = $"sul on kaks arvu a = {a} ja b = {b}";
            Console.WriteLine(välja);

            Console.WriteLine($"sul on kaks arvu a = {a} ja b = {b} ja nende korrutis on {a * b}");

            // string.format("sul on kaks arvu a = %d ja b = %d", a, b); // selline oleks java maailmas

            //StringBuilder sb = new StringBuilder();
            //sb.Append("sul on kaks arvu a = ");
            //sb.Append(a.ToString());
            //sb.Append(" ja b = ");
            //sb.Append(b.ToString());
            //välja = sb.ToString(); 

            #endregion

            //const int SÕRMEDE_ARV = 5;

            Console.Write("mis on sinu nimi: ");
            string loetudNimi = Console.ReadLine();

            //if (loetudNimi == "Henn") loetudNimi = "Peremees";

            string vahepealne = loetudNimi == "Henn" ? "ilus hommik ju" : "käi jala";

            Console.WriteLine($"Tere {loetudNimi}!");
            Console.WriteLine(vahepealne);

            // järgmine teema on juba KEERULLINE - programmiplokid ...
            // esimene kõige lihtsam neist - if

            Console.Write("mis päev täna on: ");
            string päev = Console.ReadLine();
            #region iffidega
            /*
               if (päev == "laupäev")
               {
                   // siia laupäevased tegevused
                   Console.WriteLine("toodagu õlut");
                   Console.WriteLine("tehtagu sauna");
               }
               else if (päev == "pühapäev")
               {
                   Console.WriteLine("siis joome lihtsalt õlutit");
               }

               else
               {
                   Console.WriteLine("no siis läheme tööle");
               }
               */ 
            #endregion

            switch (päev)
            {
                case "laupäev": Console.WriteLine("Täna joome õlut");
                                Console.WriteLine("Täna teeme sauna");
                    //break;
                    goto case "pühapäev";

                case "esmaspäev": Console.WriteLine("väike peaparandus"); goto default;

                case "kolmapäev":
                case "reede": Console.WriteLine("võta kaasa trenni asjad"); goto default;

                case "pühapäev": Console.WriteLine("Täna vedeleme niisama");
                    break;
                default: Console.WriteLine("täna läheme tööle");
                    break;
            }





            Console.Write("Anna keisri nimi: ");
            string keiser = Console.ReadLine().ToLower();
            string vastus = "";

            #region Elvisega
            //vastus =
            //      keiser == "napoleon" ? "Prantsuse"
            //  : keiser == "cesar" ? "Rooma"
            //  : keiser == "karl suur" ? "Saksa-Rooma"
            //  : keiser == "donald" ? "Disney"
            //  : keiser == "aleksander suur" ? "Makedoonia"
            //  : "tundmatu";
            #endregion


            #region if-lausetega

            //if (keiser == "napoleon") vastus = "Prantsuse";
            //else if (keiser == "cesar") vastus = "Rooma";
            //else if (keiser == "karl suur") vastus = "Saksa-Rooma";
            //else if (keiser == "aleksander suur") vastus = "Makedoonia";
            //else if (keiser == "donald") vastus = "Disney";
            //else if (keiser == "alice") vastus = "Imedemaa";

            //else vastus = "tundmatu";



            #endregion

            #region switchiga

            switch (keiser)
            {
                case "napoleon": vastus = "Prantsuse"; break;
                case "cesar": vastus = "Rooma"; break;
                case "karl suur": vastus = "Saksa-Rooma"; break;
                case "aleksander suur": vastus = "Makedonia"; break;
                default: vastus = "tundmatu"; break;
            }
            #endregion



            Console.WriteLine($"Ta on {vastus} riigi keiser");

        }
    }
}
